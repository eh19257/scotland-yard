package uk.ac.bris.cs.scotlandyard.model;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.util.*;
import javax.annotation.Nonnull;
import uk.ac.bris.cs.scotlandyard.model.Board.GameState;
import uk.ac.bris.cs.scotlandyard.model.Move.*;
import uk.ac.bris.cs.scotlandyard.model.Piece.*;
import uk.ac.bris.cs.scotlandyard.model.ScotlandYard.*;

import static uk.ac.bris.cs.scotlandyard.model.Piece.MrX.MRX;

/**
 * cw-model
 * Stage 1: Complete this class
 */
public final class MyGameStateFactory implements Factory<GameState> {

	@Nonnull @Override public GameState build(
			GameSetup setup,
			Player mrX,
			ImmutableList<Player> detectives) {
		// TODO
		//throw new RuntimeException("Implement me! no bitch");

		return new MyGameState(setup, ImmutableSet.of(MrX.MRX),	ImmutableList.of(), mrX, detectives);

	}

	private final class MyGameState implements GameState {

		private GameSetup setup;
		private ImmutableSet<Piece> remaining;
		private ImmutableList<LogEntry> log;
		private Player mrX;
		private List<Player> detectives;
		private ImmutableList<Player> everyone;
		private ImmutableSet<Move> moves;
		private ImmutableSet<Piece> winner;

		private boolean gameEndedFlag = false;

		private MyGameState(final GameSetup setup,
							final ImmutableSet<Piece> remaining,
							final ImmutableList<LogEntry> log,
							final Player mrX,
							final List<Player> detectives) {


			if (mrX == null) throw new NullPointerException();
			if (setup.graph.nodes().isEmpty()) throw new IllegalArgumentException();
			if (setup.rounds.isEmpty() || remaining.isEmpty()) throw new IllegalArgumentException();
			if (detectives.isEmpty()) throw new NullPointerException();


			Set<Piece> allDetectiveColours = new HashSet<Piece>();

			for (final var p : detectives) {
				if (p == null) throw new NullPointerException();
				if(p.isMrX()) throw new IllegalArgumentException();
				if (p.has(Ticket.DOUBLE) || p.has(Ticket.SECRET)) throw new IllegalArgumentException();
				allDetectiveColours.add(p.piece());
			}

			if (getAllDetectiveLocations(detectives).size() != detectives.size()) throw new IllegalArgumentException();
			if (allDetectiveColours.size() != detectives.size()) throw new IllegalArgumentException();
			if (!(mrX.piece() == MrX.MRX)) throw new IllegalArgumentException();

			//check there is only one mister x


			this.setup = setup;
			this.remaining = remaining;
			this.log = log;
			this.mrX = mrX;
			this.detectives = detectives;

			if(getAvailableMovesPara(playerListToPieceImmutableSet(detectives)).isEmpty()){
				setWinner(ImmutableSet.of(MRX));
				setGameEndedFlag(true);
			}

		}


		//HELPERS


		public Set<Integer> getAllDetectiveLocations(List<Player> detectives){
			Set<Integer> locations = new HashSet<Integer>();

			for (final var i : detectives) {
				locations.add(i.location());
			}
			return locations;
		}


		@Override public GameSetup getSetup() {
			return setup;
		}

		public ImmutableSet<Player> getEveryone(){
			Set<Player> allPlayers = new HashSet<Player>();
			allPlayers.add(mrX);

			for (final var p : detectives) {
				allPlayers.add(p);
			}
			return ImmutableSet.copyOf(allPlayers);
		}


		//NB: \/\/\/ is static in the notes
		private ImmutableSet<SingleMove> makeSingleMoves(GameSetup setup, List<Player> detectives, Player player, int source) {
			final var singleMoves = new ArrayList<SingleMove>();
			final var locations =  getAllDetectiveLocations(detectives);

			for (int destination : setup.graph.adjacentNodes(source)) {
				var occupied = false;
				if (locations.contains(destination)) {occupied = true;}
				if (occupied) continue;

				for (Transport t : setup.graph.edgeValueOrDefault(source, destination, ImmutableSet.of())) {
					if (player.has(t.requiredTicket()))
						singleMoves.add(new SingleMove(player.piece(), source, t.requiredTicket(), destination));
					if (player.has(Ticket.SECRET)){
						singleMoves.add(new SingleMove(player.piece(), source, Ticket.SECRET, destination));
					}
				}
			}
			return ImmutableSet.copyOf(singleMoves);
		}


		private ImmutableSet<DoubleMove> makeDoubleMoves(GameSetup setup,  List<Player> detectives, Player player, ImmutableSet<SingleMove> singleMoves){
			final var doubleMoves = new ArrayList<DoubleMove>();

			for (SingleMove i : singleMoves){
				var twoOrMoreTickets = player.hasAtLeast(i.ticket,2);
				for (SingleMove j : makeSingleMoves(setup, detectives, player, i.destination)){
					if ( !( !(twoOrMoreTickets) && (j.ticket == i.ticket) ) ) {
						doubleMoves.add(new DoubleMove(player.piece(), player.location(), i.ticket, i.destination, j.ticket, j.destination));
					}
				}
			}
			return ImmutableSet.copyOf(doubleMoves);
		}


		private ImmutableSet<Piece> playerListToPieceImmutableSet(List<Player> players){
			Set<Piece> pieces = new HashSet<Piece>();
			for (Player p : players){
				pieces.add(p.piece());
			}
			return ImmutableSet.copyOf(pieces);
		}


		private ImmutableSet<Player> pieceSetToPlayerSet(ImmutableSet<Piece> pieces){
			Set<Player> players = new HashSet<Player>();

			for (Player p : getEveryone()){
				if (pieces.contains(p.piece())) {
					players.add(p);
				}
			}
			return ImmutableSet.copyOf(players);
		}

		private Player pieceToPlayer(Piece piece){
			for (Player p : getEveryone()){
				if (p.piece() == piece) {
					return p;
				}
			}
			throw new IllegalArgumentException("Woops");
		}

		private List<LogEntry> makeSingleLogEntry(GameSetup setup, int index, Ticket ticket, int location){
			List newEntry = new ArrayList<LogEntry>();

			if (setup.rounds.get(index)){
				newEntry.add(LogEntry.reveal(ticket, location));
			}
			else {
				newEntry.add(LogEntry.hidden(ticket));
			}
			return newEntry;
		}

		private List<LogEntry> makeDoubleLogEntry(GameSetup setup, int index, Ticket ticket1, int location1, Ticket ticket2, int location2){
			List newEntry = new ArrayList<LogEntry>();
			newEntry.addAll(makeSingleLogEntry(setup, index, ticket1, location1));
			newEntry.addAll(makeSingleLogEntry(setup, index + 1, ticket2, location2));
			return newEntry;
		}


		private ImmutableList<LogEntry> updateLog(GameSetup setup, ImmutableList<LogEntry> oldLog, Move move){
			List newLog = new ArrayList<LogEntry>();
			newLog.addAll(oldLog);
			newLog.addAll(move.visit(new FunctionalVisitor<List<LogEntry>>(x -> makeSingleLogEntry(setup, oldLog.size(), x.ticket, x.destination), x -> makeDoubleLogEntry(setup, oldLog.size(), x.ticket1, x.destination1, x.ticket2, x.destination2))));

			return ImmutableList.copyOf(newLog);
		}

		private ImmutableSet<Move> getAvailableMovesPara(ImmutableSet<Piece> pieces){
			var temp = remaining;
			remaining = pieces;
			var store = getAvailableMoves();
			remaining = temp;
			return store;
		}

		public void setWinner (ImmutableSet<Piece> pieces){
			this.winner = pieces;
		}

		public void setGameEndedFlag (boolean val){
			this.gameEndedFlag = val;
		}

		//GETTERS


		@Override public ImmutableSet<Move> getAvailableMoves() {
			Set<Move> availableMoves = new HashSet<Move>();

			for (Player i : pieceSetToPlayerSet(remaining)){
				ImmutableSet<SingleMove> singleMoves = makeSingleMoves(setup, detectives, i, i.location());

				for (Move j : singleMoves) {
					availableMoves.add(j);
				}

				if(( i.isMrX() ) && ( i.has(Ticket.DOUBLE) ) && ( setup.rounds.size() > 1)){
					for (Move k : makeDoubleMoves(setup, detectives, i, singleMoves)) {
						availableMoves.add(k);
					}
				}
			}
			if (gameEndedFlag) return ImmutableSet.of();
			return ImmutableSet.copyOf(availableMoves);
		}



		@Override public ImmutableList<LogEntry> getMrXTravelLog() { return log; }


		@Override public ImmutableSet<Piece> getPlayers() {
			Set<Piece> allPlayers = new HashSet<Piece>();
			allPlayers.add(mrX.piece());

			for (final var p : detectives) {
				allPlayers.add(p.piece());
			}
			return ImmutableSet.copyOf(allPlayers);
		}



		@Override public Optional<Integer> getDetectiveLocation(Piece.Detective detective) {
			for (final var p : detectives) {
				if (p.piece() == detective) return Optional.of(p.location());
			}
			return Optional.empty();
		}


		@Override public Optional<Board.TicketBoard> getPlayerTickets(Piece piece) {
			if (mrX.piece() == piece) return Optional.ofNullable(mrX.tickets()).map(tickets -> ticket -> tickets.getOrDefault(ticket, 0));

			for (final var p : detectives) {
				if (p.piece() == piece ) return Optional.ofNullable(p.tickets()).map(tickets -> ticket -> tickets.getOrDefault(ticket, 0));
			}
			return Optional.empty();
		}


		@Override public ImmutableSet<Piece> getWinner() {
			if (winner == null) return ImmutableSet.of();
			else return winner;
		}



		@Override public GameState advance(Move move) {
			if ( (!getAvailableMoves().contains(move)) ) throw new IllegalArgumentException("Illegal move: " + move);

			var player = pieceToPlayer(move.commencedBy());
			player = player.use(move.tickets());


			//check if remaining contains piece\/\/
			if (!remaining.contains(move.commencedBy())){
				return new MyGameState(setup, remaining, log, mrX, detectives);
			}

			player = player.at(move.visit(new FunctionalVisitor<Integer>(x -> x.destination, x -> x.destination2)));		//updates new Location

			Set<Piece> newRemaining = new HashSet<Piece>();
			newRemaining.addAll(remaining);
			newRemaining.remove(move.commencedBy());				//removes current advanced player/piece

			List<Player> newDetectives = new ArrayList<Player>();
			newDetectives.addAll(detectives);						//create new detective list to update old

			List<LogEntry> newLog = new ArrayList<LogEntry>();
			newLog = log;

			if (player.piece() == MRX) {                            //removes mrX and adds detectives

				for (Player p : newDetectives) {
					newRemaining.add(p.piece());
				}

				if (getAvailableMovesPara(ImmutableSet.copyOf(newRemaining)).isEmpty()){
					setWinner(ImmutableSet.of(MRX));			//If detective can't move - MrX wins
					setGameEndedFlag(true);
				}

				newLog = updateLog(setup, log, move);
				mrX = player;                						//update mrX
			}
			else {
				if (player.location() == mrX.location()){
					setWinner(playerListToPieceImmutableSet(detectives));		//If detective has the same location as MrX - detectives win
					newRemaining = ImmutableSet.of(MRX);
					setGameEndedFlag(true);
				}

				mrX = mrX.give(move.tickets());
				for (Player p : detectives){						//updates detectives list
					if (p.piece().equals(player.piece())){
						newDetectives.remove(p);
						newDetectives.add(player);
						break;
					}
				}
			}

			if( (newRemaining.isEmpty()) || (getAvailableMovesPara(ImmutableSet.copyOf(newRemaining)).isEmpty())) {
				if (newRemaining.isEmpty())	newRemaining.add(MRX);			//where remaining is empty, it's refreshed


				if (setup.rounds.size() == newLog.size()) {
					setWinner(ImmutableSet.of(MRX));						//End of game and MrX hasn't been caught - MrX wins
					setGameEndedFlag(true);
				}

				if ((getAvailableMovesPara(ImmutableSet.copyOf(newRemaining)).isEmpty()) && (makeSingleMoves(setup, newDetectives, mrX, mrX.location()).isEmpty())){
					setWinner(playerListToPieceImmutableSet(detectives));		//MrX can't move - detectives win
					setGameEndedFlag(true);
				}
			}

			Set<SingleMove> remainingMoves = new HashSet<SingleMove>();

			for (Player p : newDetectives){
				remainingMoves.addAll(makeSingleMoves(setup, newDetectives, p, p.location()));
			}

			if (remainingMoves.isEmpty()){
				setWinner(ImmutableSet.of(MRX));
				setGameEndedFlag(true);
			}

			MyGameState newGameState = new MyGameState(setup, ImmutableSet.copyOf(newRemaining), ImmutableList.copyOf(newLog), mrX, List.copyOf(newDetectives));
			newGameState.setWinner(getWinner());
			newGameState.setGameEndedFlag(gameEndedFlag);

			return newGameState;
		}
	}
}

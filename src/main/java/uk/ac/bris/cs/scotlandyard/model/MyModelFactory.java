package uk.ac.bris.cs.scotlandyard.model;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.util.*;
import com.google.common.collect.ImmutableSet;
import uk.ac.bris.cs.scotlandyard.model.ScotlandYard.Factory;
import uk.ac.bris.cs.scotlandyard.model.Board.GameState;
import uk.ac.bris.cs.scotlandyard.model.Model.Observer.Event;

/**
 * cw-model
 * Stage 2: Complete this class
 */
public final class MyModelFactory implements Factory<Model> {

	@Nonnull @Override public Model build(GameSetup setup,
	                                      Player mrX,
	                                      ImmutableList<Player> detectives) {

		return new MyModel(setup, ImmutableSet.of(Piece.MrX.MRX), ImmutableList.of(), mrX, detectives);
	}

	private final class MyModel implements Model {
		private ImmutableSet<Observer> observers = ImmutableSet.of();
		private GameState state;

		private MyModel(final GameSetup setup, final ImmutableSet<Piece> remaining, final ImmutableList<LogEntry> log, final Player mrX, final List<Player> detectives){
			state = new MyGameStateFactory().build(setup, mrX, ImmutableList.copyOf(detectives));
		}

		@Nonnull
		public Board getCurrentBoard() {
			return state;
		}

		public void registerObserver(@Nonnull Model.Observer observer) {
			if (observer.equals(null)) throw new IllegalArgumentException();
			if (observers.contains(observer)) throw new IllegalArgumentException();

			Set<Observer> newObservers = new HashSet<Observer>();
			newObservers.addAll(observers);
			newObservers.add(observer);
			observers = ImmutableSet.copyOf(newObservers);
		}

		public void unregisterObserver(@Nonnull Model.Observer observer) {
			if (observer.equals(null)) throw new IllegalArgumentException();
			if (!observers.contains(observer)) throw new IllegalArgumentException();

			Set<Observer> newObservers = new HashSet<Observer>();
			newObservers.addAll(observers);
			newObservers.remove(observer);
			observers = ImmutableSet.copyOf(newObservers);
		}

		@Nonnull public ImmutableSet<Model.Observer> getObservers() {
			return observers;
		}

		@Override public void chooseMove(@Nonnull Move move) {
			state = state.advance(move);
			var event = state.getWinner().isEmpty() ? Event.MOVE_MADE : Event.GAME_OVER;
			for (Observer o : observers) {
				o.onModelChanged(getCurrentBoard(), event);
			}
		}
	}
}
